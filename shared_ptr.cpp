struct Expression;
struct Number;
struct BinaryOperation;

struct Storage {
    
    explicit Storage(Expression* ptr): ptr_(ptr), count(1) {}
    
    ~Storage() {
        delete ptr_;
    }
    
    bool dec() {
        if(--count == 0) {
            return true;
        }
        return false;
    }
    
    Expression* ptr_;
    std::size_t count;
};

struct SharedPtr
{   
    explicit SharedPtr(Expression *ptr = 0): storage(0) {
        if(ptr != 0) {
            storage = new Storage(ptr);
        }
    }
    
    ~SharedPtr() {
        if(storage != 0) {
            remove();
        }
    }
    
    SharedPtr(const SharedPtr &rhs): storage(0) {
        if(rhs.storage != 0) {
            storage = rhs.storage;
            ++storage->count;
        }
    }
    
    SharedPtr& operator=(const SharedPtr &rhs) {
        
        if(storage == rhs.storage) {
            return *this;
        }
        
        if(storage != 0) {
            remove();
        }
        if(rhs.storage != 0) {
            storage = rhs.storage;
            ++storage->count;
        }
        
    }
    
    Expression* get() const {
        if(storage != 0) {
            return storage->ptr_;
        }
        return 0;
    }
    
    void reset(Expression *ptr = 0) {
        if(storage != 0) {
            remove();
        }
        if(ptr != 0) {
            storage = new Storage(ptr);
        }
    }
    
    Expression& operator*() const {
        return *(get());
    }
    Expression* operator->() const {
        return get();
    }
    
private:
    
    void remove() {
        if(storage->dec()) {
            delete storage;
        }
        storage = 0;
    }
    
    Storage* storage;
};

