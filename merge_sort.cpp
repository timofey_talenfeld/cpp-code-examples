#include <iostream>
#include <vector>
#include <algorithm>

std::size_t count = 0;

std::vector< int > merge(std::vector< int > left, std::vector< int > right) {
    
    std::size_t l = 0;
    std::size_t r = 0;
    std::vector< int > sorted_array;
    sorted_array.reserve(left.size() + right.size());
    
    while(l != left.size() && r != right.size()) {
        if(left[l] <= right[r]) {
            sorted_array.push_back(left[l++]);
        } else {
            sorted_array.push_back(right[r++]);
            count += left.size() - l;
        }
    }
    
    while(l < left.size()){
        sorted_array.push_back(left[l++]);
    }
    
    while(r < right.size()) {
        sorted_array.push_back(right[r++]);
    }
    
    return sorted_array;
}

std::vector< int > sort(std::vector< int > &array) {
    
    if(array.size() == 1) {
        return array;
    }
    
    std::size_t middle = array.size() / 2;
    
    std::vector< int > left(array.begin(), array.begin() + middle);
    std::vector< int > right(array.begin() + middle, array.end());
    
    return merge(sort(left),sort(right));
}

int main() {
    std::size_t n = 0;
    std::cin >> n;
    std::vector< int > array(n);
    
    for(std::size_t i = 0; i < n; ++i) {
        std::cin >> array[i];
    }
    
    array = sort(array);
    
    std::cout << count;
    
    return 0;
}

