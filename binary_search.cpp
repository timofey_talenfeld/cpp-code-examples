#include <iostream>

int binarySearch(int* array, std::size_t n, int k) {
    
    std::size_t l = 1;
    std::size_t r = n;
    
    while(l <= r) {
        std::size_t m = (l+r)/2;
        if(array[m] == k) {
            return m;
        } else if(k < array[m]) {
            r = m - 1;
        } else {
            l = m + 1;
        }
    }
    
    return -1;
}

int main() {
    std::size_t n = 0;
    std::cin >> n;
    int* array = new int[n+1];
    array[0] = 0;
    
    for(std::size_t i = 1; i <= n; ++i) {
        std::cin >> array[i];
    }
    
    std::size_t k = 0;
    std::cin >> k;
    
    for(std::size_t i = 0; i < k; ++i) {
        int tmp = 0;
        std::cin >> tmp;
        std::cout << binarySearch(array,n,tmp) << ' ';
    }
    
    delete [] array;
    return 0;
}

