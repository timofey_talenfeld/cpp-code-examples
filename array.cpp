#include <cstddef>

template <typename T>
class Array
{
public:
    // Список операций:
    //
    // explicit Array(size_t size = 0, const T& value = T())
    //   конструктор класса, который создает
    //   Array размера size, заполненный значениями
    //   value типа T. Считайте что у типа T есть
    //   конструктор, который можно вызвать без
    //   без параметров, либо он ему не нужен.
    //
    
    explicit Array(size_t size = 0, const T &value = T()): size_(size) {
        array_ = new T[size_];
        for(size_t i = 0; i < size_; ++i) {
            array_[i] = value;
        }
    }
    
    // Array(const Array &)
    //   конструктор копирования, который создает
    //   копию параметра. Считайте, что для типа
    //   T определен оператор присваивания.
    //
    
    Array(const Array &other) {
        size_ = other.size();
        array_ = new T[size_];
        for(size_t i = 0; i < size_; ++i) {
            array_[i] = other.array_[i];
        }
    }
    
    // ~Array()
    //   деструктор, если он вам необходим.
    //
            
    ~Array() {
        delete[] array_;
    }
            
    // Array& operator=(...)
    //   оператор присваивания.
    //
        
    Array& operator=(const Array& lhs) {
        if(this != &lhs) {
            size_ = lhs.size();
            delete[] array_;
            array_ = new T[size_];
            for(size_t i = 0; i < size_; ++i) {
                array_[i] = lhs.array_[i];
            }
        }
        return *this;
    }    
        
    // size_t size() const
    //   возвращает размер массива (количество
    //                              элементов).
    //
        
    size_t size() const {
        return size_;
    }
        
    // T& operator[](size_t)
    
    T& operator[](size_t index) {
        return array_[index];
    }
    
    // const T& operator[](size_t) const
    //   две версии оператора доступа по индексу.

    const T& operator[](size_t index) const {
        return array_[index];
    }        
        
private:
    T *array_;
    size_t size_;
};

