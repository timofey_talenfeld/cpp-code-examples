#include <iostream>
#include <vector>

unsigned int M = 10;

int main() {
    std::size_t n = 0;
    std::cin >> n;
    
    std::vector< unsigned int > array;
    array.reserve(n);
        
    for(std::size_t i = 0; i < n; ++i) {
        unsigned int tmp = 0;
        std::cin >> tmp;
        array.push_back(tmp-1);
    }
    
    std::vector< std::size_t > counted_array(M);
    
    for(std::size_t i = 0; i < array.size(); ++i) {
        ++counted_array[array[i]];
    }
    
    for(std::size_t i = 1; i < counted_array.size(); ++i) {
        counted_array[i] += counted_array[i-1];
    }
    
    std::vector< unsigned int > sorted_array(n);
    
    for(std::size_t i = 0; i < array.size(); ++i) {
    	--counted_array[array[array.size()-i-1]];
        sorted_array[counted_array[array[array.size()-i-1]]] = array[array.size()-i-1];
    }
    
    for(std::size_t i = 0; i < sorted_array.size(); ++i) {
        std::cout << sorted_array[i]+1 << ' ';
    }
    
    return 0;
}

